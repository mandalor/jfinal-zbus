/**
 * Copyright (c) 2015, 玛雅牛［李飞］ (myaniu@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfinal.plugin.zbus.coder;

import org.zbus.net.http.Message;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * @ClassName: JsonCoder
 * @Description: 基于Json实现的编码解码器
 * @author 李飞
 * @param <T>
 * @date 2015年8月11日 上午1:43:25
 * @since V1.0.0
 */
public class JsonCoder implements Coder {

	public JsonCoder() {
	}

	@Override
	public Message encode(Object obj) {
		Message msg = new Message();
		msg.setBody(JSON.toJSONString(obj, SerializerFeature.WriteClassName, SerializerFeature.IgnoreNonFieldGetter));
		return msg;
	}
	
	@Override
	public <T> T decode(Class<T> tClass, Message msg) throws Exception {
		String textMsg = msg.getBodyString();
		return JSON.parseObject(textMsg, tClass);
	}
}
